Mikrob2
=======

Next generation.
----------------

Tweetdeck-like client for Blip.pl (http://blip.pl) microblogging service.

Mikrob was originally written by Łukasz Korecki (http://coffeesounds.com/)

Icons by [GentleFace](http://www.gentleface.com/free_icon_set.html)
