var Platform = (function() {
	function _openUrlTitanium(url) {
		Titanium.Desktop.openURL(url);
	}
	
	function _openUrlChrome(url) {
		chrome.tabs.create({
			url : url
		} );
	}

	function openURL(url) {
		if (window.Titanium) {
			_openUrlTitanium(url);
		}
		if (window.chrome) {
			_openUrlChrome(url);
		}
	}

	function templatePath(name) {
		var path = "app://source/templates/" + name + '.mustache';
		if (window && window.chrome) {
			path = chrome.extension.getURL('templates/' + name + '.mustache');
		}
		if (!window) {
			path = '../templates/' + name + '.mustache';
		}
		return path;
	}

	function installOmnibox() {
		if(window.chrome) {
			chrome.omnibox.setDefaultSuggestion(
			{
				description: "Pozostało znaków: <match>" + 160 + "</match>"
			}
			);

			chrome.omnibox.onInputEntered.addListener(function(text) {
				var no;
				Mikrob.Service.createStatus(text, no, {
					onSuccess : function() {
						Mikrob.Service.updateDashboard(Mikrob.Controller.viewport);
					},
					onFailure : function() {
						var msg = 'Wysłanie nie powiodło się. ';
						Mikrob.Notification.create('', msg);
					}
				});
			});

			chrome.omnibox.onInputChanged.addListener(function(text, suggest) {
				chrome.omnibox.setDefaultSuggestion(
				{
					description: "Pozostało znaków: <match>" + (160 - text.length) + "</match>"
					}
				);
			});
		}
	}

	return {
		openURL : openURL,
		templatePath : templatePath,
		installOmnibox : installOmnibox
	};
})();
